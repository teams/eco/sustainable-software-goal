# Awesome sustainable software

This is a curated list of awesome resources about sustainable software. It follows the spirit and format of the popular ["awesome lists"](https://github.com/sindresorhus/awesome).

The scope of this list includes everything that contributes to the understanding of sustainable software, including resources that assist in its analysis, design, and implementation. The list was created from a perspective of Free Software but is meant to also be generally useful for any other kind of software. Rather than serving as a catalog of concrete software applications, this list primarily focuses on research, concepts, guidelines, and tools. Specific vendor-specific commercial solutions are out of scope of this list.

The list is open for contributions. If you know useful resources which are not on the list yet, don't hesitate to create a merge request adding them. Please follow the format of the other entries.

## What is sustainable software?

Sustainable software is software that is designed and developed with long-term maintainability, efficiency, and usefulness in mind. This means that the software is built with robust and clean code, follows industry best practices, and is designed to be easily understood and modified by future developers.

Additionally, sustainable software is often designed to be energy efficient and have a low impact on the environment, making it a more environmentally friendly choice. By focusing on sustainability, organizations can save money on maintenance and updates in the long run, while also doing their part to protect the environment.

See also: "[What is Sustainable Software, After All?](https://eco.kde.org/blog/2023-05-10-what-is-sustainable-software/)" (Cornelius Schumacher, 2023-05-10)

## Resources

### Initiatives

* [Agile Sustainability Initiative](https://www.agilealliance.org/resources/initiatives/agile-sustainability-initiative/) - Manifesto and resources about how to augment agile principles by sustainability aspects

### Software

#### Lists

 * [Open Sustainable Technology](https://opensustain.tech/) - Comprehensive list of open source projects in environemntal sustainability
 * [Green Software](https://github.com/Green-Software-Foundation/awesome-green-software) - List of research, tools, code, libraries, and training for building environmentally sustainable software by Green Software Foundation
 * [DDSC's Sustainable Data Science Guide](https://github.com/Dansk-Data-Science-Community/sustainable-data-science) - List of resources on sustainable data science

#### Projects

 * [kube-green](https://github.com/kube-green/kube-green) - Kubernetes addon that automatically shuts down resources
 * [CO2.js](https://developers.thegreenwebfoundation.org/co2js/overview/) - Tool for estimating emissions of apps, websites, software by Green Web Foundation
 * [Mojo](https://www.fast.ai/posts/2023-05-03-mojo-launch.html) - New programming language meant to be a more efficient version of Python for AI developers, by creator of LLVM and Swift

### Software-Driven Energy Consumption

#### Sustainable Software Design

* [The Karlskrona Manifesto for Sustainability Design](https://www.sustainabilitydesign.org/) - Thoughts about principles and commitments for sustainable design
* [The era of green software](https://yewtu.be/watch?v=xtQOxGtmhy4) - Talk about sustainability in the world of software and computing

#### Green Coding

* [Green Coding](https://www.gft.com/int/en/technology/greencoding) - Paper about green coding concepts

#### Measurement Tools

 * [Green Coding Measuring Tools](https://github.com/schaDev/GreenCoding-measuring-tools) - Overview of tools to measure energy consumption and carbon emissions of software
 * [Green Metrics Tool](https://github.com/green-coding-berlin/green-metrics-tool) - Tool to measure resource usage of software by Green Coding Berlin
 * [German: "Energieverbrauch von Software: Eine Anleitung zum Selbermessen"](https://blog.oeko.de/energieverbrauch-von-software-eine-anleitung-zum-selbermessen/) - Instructions how to do a basic measurement of energy consumption of software

#### Research

 * [Sustainable software products -- Towards assessment criteria for resource and energy efficiency](https://doi.org/10.1016/j.future.2018.02.044) (Kern et al., 2018) - Reseach about sustainability criteria for software which also was the base for the Blue Angel criteria
 * German: [Entwicklung und Anwendung von Bewertungsgrundlagen für ressourceneffiziente Software unter Berücksichtigung bestehender Methodik](https://www.umweltbundesamt.de/publikationen/entwicklung-anwendung-von-bewertungsgrundlagen-fuer) (Abschlussbericht, 2018) - Report from the research which led to the recommendation for the Blue Angel eco label for resource efficient software
 * German: [Smarte Grüne Welt: Digitalisierung zwischen Überwachung, Konsum und Nachhaltigkeit](https://www.oekom.de/buch/smarte-gruene-welt-9783962380205) (Steffen Lange und Tilman Santarius, 2018]) - Book about social and ecological impact of digital transformation
 * [On Global Electricity Usage of Communication Technology: Trends to 2030](https://doi.org/10.3390/challe6010117) - Study from 2015 estimating development of energy usage of communications technology
 * [Energy efficiency across programming languages: how do energy, time, and memory relate?](https://greenlab.di.uminho.pt/wp-content/uploads/2017/10/sleFinal.pdf) - Often cited study about energy efficiency of different programming languages, should be applied with care

#### Estimations

* [Environmental impact assessment of online advertising](https://doi.org/10.1016/j.eiar.2018.08.004) - Study estimating that online advertising contributes 10% of global emissions of the Internet
* [The Carbon Footprint of ChatGPT](https://towardsdatascience.com/the-carbon-footprint-of-chatgpt-66932314627d) - Possible scenario for estimating ChatGPT's carbon footprint
* [Does not compute: Avoiding pitfalls assessing the Internet's energy and carbon impacts](https://doi.org/10.1016/j.joule.2021.05.007) - Paper about problems of energy and carbon footprint estimations

#### Transparency

* [HTTP Response Header Field: Carbon-Emissions-Scope-2](https://datatracker.ietf.org/doc/draft-martin-http-carbon-emissions-scope-2/) - Proposal for a HTTP header making carbon emissions of API calls transparent

### Primers / Courses

 * [Green Software Practitioner](https://learn.greensoftware.foundation/) - Training for software developers how to reduce carbon emissions caused by software by the Green Software Foundation
 * [Sustainable Software Engineering](https://open.hpi.de/courses/sustainablesoftware2022) - Online course about resource efficient software engineering by the Hasso Plattner Institute

### Blog Posts / Articles

 * [Climate-friendly software: don't fight the wrong battle](https://blog.ltgt.net/climate-friendly-software/) - Considerations about relative impact of measures to reduce carbon emissions related to software
 * [Climate-friendly coding](https://us11.campaign-archive.com/?e=01d2b095a0&u=9d7ced8c4bbd6c2f238673f0f&id=35bdf59d6a) - Q&A with Niki Manoledaki, Tejas Chopra, Arne Tarara as part of ReadME Project
 * [AI is harming our planet: addressing AI's staggering energy cost](https://www.numenta.com/blog/2022/05/24/ai-is-harming-our-planet/) - Article about energy costs of AI models and ways how to reduce them
 * [The mounting human and environmental costs of generative AI](https://arstechnica.com/gadgets/2023/04/generative-ai-is-cool-but-lets-not-forget-its-human-and-environmental-costs/) - Critical look on social and environmental impact of large language models

### Sustainability Reports

 * [Computing And Climate Change](https://dl.acm.org/doi/pdf/10.1145/3483410) (ACM Technology Policy Council, 2021]) - Report about impact of information and communication technology on carbon output and how to address it
 * [Carbon footprint of unwanted data-use by smartphones: An analysis for the EU](https://groenlinks.nl/sites/groenlinks/files/2021-09/CE_Delft_210166_Carbon_footprint_unwanted_data-use_smartphones.pdf) (2021) - Study which estimates impact of user tracking on carbon emissions to be equivalent to the emissions of about half a million EU citizen
 * [Lean ICT: Towards Digital Sobriety](https://theshiftproject.org/wp-content/uploads/2019/03/Lean-ICT-Report_The-Shift-Project_2019.pdf) (The Shift Project, 2019) - Recommendations how to minimze the damaging effects of digital transition on the environment
 * [E-Waste Monitor](https://ewastemonitor.info/) - Studies about electronic waste by the United Nations and the International Telecommunication Union

### Sustainable AI

#### Classification schemes

* [Ethical AI Rating](https://nextcloud.com/blog/nextcloud-ethical-ai-rating/) - Criteria for rating ethical aspects of AI models
* [ML Policy](https://salsa.debian.org/deeplearning-team/ml-policy/-/blob/master/ML-Policy.rst) - Classification for AI artifacts from software freedom point of view 
* [Open Source AI](https://opensource.org/deepdive) - OSI's initiative to define "Open Source AI"

#### Running AI locally

* [Ollama](https://ollama.com/) - Tool for running large language models locally
* [LocalAI](https://localai.io/) - Local drop-in replacement for OpenAI API
* [llamafile](https://github.com/Mozilla-Ocho/llamafile) - Distribute models in a single executable file

### Related Resources

#### Carbon Calculators & Similar

 * [Greenhouse Gas Protocol](https://ghgprotocol.org/) - Widely used standard to report greenhouse gas emissions
 * [Carbon Calculator](https://www.carbonfootprint.com/calculator.aspx) - Calculate carbon footprint for individuals and households
 * [Image Carbon](https://www.imagecarbon.com/) - Calculate carbon footprint of websites, based on CO2.js
 * [Green Web Check](https://www.thegreenwebfoundation.org/) - Check if web site hoster uses green energy / compensation
 * [Cloud Carbon Footprint](https://www.cloudcarbonfootprint.org/) - Tool to estimate carbon footprint of cloud workloads
 * [Green Algorithms](https://www.green-algorithms.org/) - Calculator of carbon footprint of computations

#### Data Centers

 * [Heat Reuse](https://www.opencompute.org/projects/heat-reuse) - Working group of the Open Compute Project to reuse heat of data centers

#### Sustainability In FOSS Organizations

 * [Environmental sustainability at GitLab](https://about.gitlab.com/handbook/esg/) - How GitLab quantifies and offsets carbon emissions
 * [Mozilla’s Climate Commitments](https://blog.mozilla.org/en/mozilla/mozillas-climate-commitments/) - How Mozilla addressed their responsibility for carbon emissions in 2020

#### Carbon Emissions Of FOSS Events

 * [Mini-GUADEC 2022 Berlin: retrospective](https://tecnocode.co.uk/2022/08/05/mini-guadec-2022-berlin-retrospective/) - Report from GNOME meeting including considerations on climate impact
 * [GUADEC 2020](https://tecnocode.co.uk/2020/09/14/guadec-2020/) - Report from GNOME meeting including considerations on climate impact
